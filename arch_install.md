# arch install 

## flatpak

    sudo pacman -S \
        flatpak \
        xdg-desktop-portal-gtk

## wayland

    sudo pacman -S \
        wl-clipboard

## systemd initcpio fix
    
    sudo touch /etc/systemd/do-not-udevadm-trigger-on-update

## applications

### buku

    python -m venv ~/.venv/buku
    ~/.venv/buku/bin/pip install buku
    ln -s ~/.../buku/bookmarks.db ~/.local/share/buku/bookmarks.db

### pass

    pass init <GPGKEY>
    pass git init
    pass git remote add origin git@gitlab.com:*
    pass git pull --allow-unrelated-histories origin master

### yadm

    yadm clone git@gitlab.com:*
    yadm status
